$(function () {

    // required, replace the subdomain of your appplication with 'portal-sso', here is 'sso-example'
    var ssoUri = 'https://' + window.location.hostname.replace('sso-example', 'portal-sso');

    // required, if we can get a cookie named 'EIName', it means the user has signed in
    var EIName = Cookies.get('EIName');

    // check signed in status, (if = not signed, else = signed)
    if (typeof (EIName) === 'undefined') {
        $('button[name="signOutBtn"]').hide();

        // required, integrate SSO signed in feature
        $('button[name="signInBtn"]').click(function () {
            var signInRedirectUri = 'https://' + window.location.hostname;
            var redirectUri = ssoUri + '/web/signIn.html?redirectUri=' + signInRedirectUri;
            window.location.href = redirectUri;
        });

        $('div[name="userProfile"]').html('Please sign in.');
    } else {
        $('button[name="signInBtn"]').hide();
        $('button[name="signUpBtn"]').hide();
        $('button[name="signOutBtn"]').show();

        // required, integrate SSO signed out feature
        $('button[name="signOutBtn"]').click(function () {
            var signOutRedirectUri = 'https://' + window.location.hostname;
            var redirectUri = ssoUri + '/web/signOut.html?redirectUri=' + signOutRedirectUri;
            window.location.href = redirectUri;
        });

        // get profile of signed in user
        $.ajax({
            url: ssoUri + '/v1.3/users/me',
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
            xhrFields: {
                withCredentials: true
            }
        }).done(function (user) {
            $('a[name="currentUser"]').text('Hello! ' + user.firstName);
            $('td[name="username"]').text(user.username);
            $('td[name="firstName"]').text(user.firstName);
            $('td[name="lastName"]').text(user.lastName);
            $('td[name="role"]').text(user.role);
            $('td[name="status"]').text(user.status);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        });
    }
});
