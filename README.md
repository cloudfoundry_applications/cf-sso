 - Native sign in
`curl -i -X POST  -H "Content-Type:application/json" -d '{ "username": "teddy@iii.org.tw", "password": “1qaz@WSX" }' 'https://portal-sso.iii-cflab.com/v1.3/auth/native'`
 - Token validation
`curl -i -X POST  -H "Content-Type:application/json" -d '{  "token" : "<EIToken>"}' 'https://portal-sso.iii-cflab.com/v1.3/tokenvalidation'`